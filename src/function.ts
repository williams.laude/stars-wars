function getTimeRemaining(endtime: any) {
  //@ts-ignore
  var t = new Date(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  //var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    //'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id: string, endtime: Date) {
  let clock = document.getElementById(id);
  let daysSpan = clock.querySelector('.days');
  let hoursSpan = clock.querySelector('.hours');
  let minutesSpan = clock.querySelector('.minutes');
  let secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    // daysSpan.innerHTML = t.days.toString();
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      console.log(timeinterval);
      
      clearInterval(timeinterval);
      localStorage.setItem('canPlay', 'true')
      hoursSpan.innerHTML = '';
      minutesSpan.innerHTML = '';
      secondsSpan.innerHTML = '';
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

export default function deadline() {
  const date = localStorage.getItem('timer')
  console.log(date)

  initializeClock('clockdiv', JSON.parse(date));
}
