const title = 'Hello TypeScript' as string;
import timers from './function'

let radId = Math.floor(Math.random() * 100);
let card = document.getElementById('Name');
let img = document.getElementById('img')
let button = document.getElementById('butt')
let categories = ['people', 'planets', 'species', 'starships', 'vehicles']
let countMax = [82, 60, 37, 36, 39]
const currentTime = Date.now();
const timeRemainig = localStorage.getItem('timer')
const parse = JSON.parse(timeRemainig)
let id = 1;
let canPlay = localStorage.getItem('canPlay') || 'true';

let Test = timers;
Test();

button.addEventListener('click', () => {

    if (localStorage.getItem('canPlay') === 'false') {
        return;
    }
    localStorage.setItem('canPlay', 'false');
    let radCat = Math.floor(Math.random() * categories.length);
    let radId = Math.floor(Math.random() * countMax[radCat]);
    let url = `https://swapi.dev/api/${categories[radCat]}/${radId}/`
    fetch(url)
        .then((data) => { return data.json() })
        .then((datas) => {
            img.innerHTML = `<img src="src/images/people.jpg" class="card-img-top" alt="people">
                     <h5 class="card-title mt-3">${datas.name}</h5>
                     <p class="card-text mb-3">${datas.gender}</p>`
            console.log(datas.detail)
            let infos = {
                name: datas.name,
                gender: datas.gender
            }
            localStorage.setItem(`Card${id}`, JSON.stringify(infos))

             //@ts-ignore
            const date = new Date(Date.parse(new Date()) + 1 * 1 * 60 * 1000);
            localStorage.setItem('timer', JSON.stringify(date))
            Test();
        })
        .catch((err) => { return console.log(err) })
})